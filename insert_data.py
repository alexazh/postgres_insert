import psycopg2
from config import config


def insert_data(test_name):
    """ insert a new test_data into the test table """
    sql = """INSERT INTO test(test_name) VALUES(%s);"""
    conn = None
    try:
        # read database configuration
        params = config()
        # connect to the PostgreSQL database
        conn = psycopg2.connect(**params)
        # create a new cursor
        cur = conn.cursor()
        # execute the INSERT statement
        cur.execute(sql, (test_name,))
        # commit the changes to the database
        conn.commit()
        # close communication with the database
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


if __name__ == '__main__':
    a = 0
    while True:
        insert_data('test_variable%s' % a)
        print('test_variable%s' % a)
        a += 1
